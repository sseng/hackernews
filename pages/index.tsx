import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import TopStoriesList from '../components/TopStoriesList'

interface Props {
  data: string[];
}

const Home = ({ data }: Props) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Hacker News App</title>
        <meta name="description" content="Sample Hacker Project" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <nav style={{
     backgroundColor: 'rgb(255, 102, 0)',
     padding: '10px 50px',
     fontWeight: 'bold'
      }}>
        Hacker News
      </nav>

      <main className={styles.main}>
      <TopStoriesList data={data} />
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}

export default Home

export const getStaticProps = async () => {
  const data = await fetch(
    "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty"
  ).then((response) => response.json());
  return {
    props: { data }
  };
};