

import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import Loading from './Loading'
import styles from '../styles/TopStories.module.css'

interface Post {
    by: string;
    descendants: number;
    id: string;
    kids: number[];
    score: number;
    time: number;
    title: string;
    type: string;
    url: string;
}
interface Props {
    data: string[];
}

const TopStoriesList = ({ data }: Props) => {
    const [pg, setPg]= useState(1)
    const [limit, setLimit] = useState(0)
    const [offset, setOffset] = useState((pg - 1) * limit)
  
  const [posts, setPosts] = useState<any>([]);
  const [hasMore, setHasMore] = useState(true);

  useEffect(() => {
    getPosts(data.slice(offset, 10))
   },[]);


 
const getPost = async (id: string) => {
   const res = await fetch(
        `https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`
      ).then((response) => response.json());
      return res
}

const getPosts = async (postIds: string[]) => {
        const postsData = await Promise.all(postIds.map((id: string) => {
        const res =  getPost(id)
        return res
    }))
 
    setPg(pg + 1)
    setLimit(10 * pg + 10)
    setOffset((pg - 1) * 10 + 10)
    setPosts((post: Post[]) => [...post, ...postsData])
  
  };

  const getMorePost = async () => {
    if(posts.length === data.length){
        setHasMore(false)
    }
    getPosts(data.slice(offset, limit))
  
  };

  return (
    <>
      <InfiniteScroll
        dataLength={posts.length}
        next={getMorePost}
        hasMore={hasMore}
        loader={<Loading />}
        endMessage={<h4>Nothing more to show</h4>}
      >
          <ol className={styles["PostsContainer"]}>
        {posts.map((data: Post, index: number) => (
          <li key={index} className={styles["Post"]}>
            <a href={data.url} target="_blank" rel="noreferrer"><strong>{data.title}</strong></a>
            <p>{data.score} points by {data.by} - id: {data.id}</p>
          </li>
        ))}
        </ol>
      </InfiniteScroll>
 
    </>
  );
};

export default TopStoriesList;

